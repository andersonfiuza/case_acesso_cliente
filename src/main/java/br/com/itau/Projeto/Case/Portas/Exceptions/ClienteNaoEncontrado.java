package br.com.itau.Projeto.Case.Portas.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "CLiente não encontrado!")
public class ClienteNaoEncontrado extends RuntimeException {

}
